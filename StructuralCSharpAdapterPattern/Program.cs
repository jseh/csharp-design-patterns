﻿using System;

namespace CSharpAdapterPattern
{
    // define la interfaz a la que se quiere convertir
    public interface ITarget
    {
        string GetRequest();
    }

    // es la clase que necesita ser adaptada y que contienen funcionalidad util
    // es la clase que queremos adaptar, para que puede ser usada por el cliente
    class Adaptee
    {
        public string GetSpecificRequest()
        {
            return "Specific request.";
        }
    }

    // es la clase que se encarga de hacer compatible la clase Adaptee
    // implementa la interface con la que se quiere hacer compatible
    class Adapter : ITarget
    {
        private readonly Adaptee _adaptee;

        public Adapter(Adaptee adaptee)
        {
            this._adaptee = adaptee;
        }

        public string GetRequest()
        {
            return $"This is '{this._adaptee.GetSpecificRequest()}'";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Adaptee adaptee = new Adaptee();
            ITarget target = new Adapter(adaptee);
            Console.WriteLine("Adaptee interface is incompatible with the client.");
            Console.WriteLine("But with adapter client can call it's method.");
            Console.WriteLine(target.GetRequest());

        }
    }
}
