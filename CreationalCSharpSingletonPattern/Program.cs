﻿using System;

namespace CSharpSingletonPattern
{

    public sealed class Singleton
    {
        public int Contador { get; set; } = 0;
        private readonly static Singleton _instance = new Singleton();
    
        // para evitar instaciacion
        private Singleton()
        {
        }
    
        public static Singleton Instance
        {
            get
            {
                return _instance;
            }
        }


    }


    public class OtroSingleton
    {
        public static OtroSingleton Instance { get; private set; }
    
        private OtroSingleton(int id, string name)
        {
            /* … */
        }
    
        public static void Init(int id, string name)
        {
            Instance = new OtroSingleton(id, name);
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            var c = Singleton.Instance;
            
            Console.WriteLine($"Mostrar contador: {c.Contador}");

            c.Contador += 1;

            var c2 = Singleton.Instance;


            Console.WriteLine($"Mostrar contador: {c2.Contador}");
        }
    }
}
