namespace StructuralCSharpDecoratorPattern
{
    // es el decorador base
    // el decorador debe ser del mismo tipo de la clase a decorar
    public class CardDecorator : Card
    {
         
        protected Card carta;

        public CardDecorator(Card carta,  string nombre,  int ataque,  int defensa ) : base( nombre, ataque, defensa)
        {
            this.carta = carta;
        }

        // sobreescribir los metodos virtuales de Card

        public override string Nombre 
        {
            get
            {
                return $"{carta.Nombre}, {nombre}";
            }
        }
        public override int Ataque 
        {
            get
            {
               return (carta.Ataque + ataque);
            }
        }
        public override int Defensa 
        {
            get
            {
                return (carta.Defensa + defensa);
            }
        }

        // public virtual void MakeTrueDamage(){}

    }
}