namespace StructuralCSharpDecoratorPattern
{
    public class Card
    {
        protected string nombre;
        protected int ataque;
        protected int defensa;

        public Card(string nombre, int ataque, int defensa)
        {
            this.nombre = nombre;
            this.ataque = ataque;
            this.defensa = defensa;
        }

        public virtual string Nombre
        {
            get
            {
                return this.nombre;
            }
        }
        public virtual int Ataque
        {
            get
            {
                return this.ataque;
            }
        }
        public virtual int Defensa
        {
            get
            {
                return this.defensa;
            }
        }
    }
}