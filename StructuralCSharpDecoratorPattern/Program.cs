﻿using System;

namespace StructuralCSharpDecoratorPattern
{

    public class AtaqueDecorator : CardDecorator
    {
        public AtaqueDecorator(Card carta, string nombre,  int ataque): base(carta, nombre, ataque, 0)
        {

        }

        // public override void MakeTrueDamage(){
        //     Console.WriteLine("true damagee!");
        // }

    }

    public class DefensaDecorator : CardDecorator
    {
        public DefensaDecorator(Card carta, string nombre, int defensa): base(carta, nombre, 0, defensa)
        {

        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            Card soldado = new Card("Soldado", 100, 50);
            Console.WriteLine($"Estatus de soldado: ataque {soldado.Ataque} / defensa {soldado.Defensa}");

            // el patron decorador  puede agregar propiedades y metodos a objetos ya instanciados
            soldado = new AtaqueDecorator(soldado, "Espada", 50 );
            // soldado.MakeTrueDamage();
            soldado = new DefensaDecorator(soldado, "Escudo", 20 );
            Console.WriteLine($"Estatus de soldado: ataque {soldado.Ataque} / defensa {soldado.Defensa}");


        }
    }
}
