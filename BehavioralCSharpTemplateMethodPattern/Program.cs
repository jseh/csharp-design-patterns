﻿using System;

namespace BehavioralCSharpTemplateMethodPattern
{

    abstract class PizzaTemplate
    {
        // el template method define el orden en el que se van hacer las cosas,  mas no como
        public virtual void PrepararPizzaTemplateMethod()
        {
            this.Cobrar();
            this.PrepararMasa();
            this.PonerIngredientes();
            this.PonerEnHorno();
        }

        protected virtual void Cobrar()
        {
            Console.WriteLine("cobrando de forma normal");
        }
        protected virtual void PrepararMasa()
        {
            Console.WriteLine("preparando masa de forma normal");
        }
        protected virtual void PonerIngredientes()
        {
            Console.WriteLine("poner ingredientes de forma normal");
        }
        protected virtual void PonerEnHorno()
        {
            Console.WriteLine("colocando en horno a temperatura normal");
        }
    }

    // la implementacion define como van hacerse las cosas
    class PizzaHawaiana : PizzaTemplate
    {
        public override void PrepararPizzaTemplateMethod()
        {
            base.PrepararPizzaTemplateMethod();
        }

        protected override void PonerIngredientes()
        {
            Console.WriteLine("ingredientes de piza hawaina");
        }

        protected override void PonerEnHorno()
        {
            Console.WriteLine("a temperatura mas alta");
        }
    }

    class PizzaSuprema : PizzaTemplate
    {
        
        
        protected override void PonerIngredientes()
        {
            Console.WriteLine("ingredientes de piza suprema");
        }

        protected override void PonerEnHorno()
        {
            Console.WriteLine("a temperatura mas baja");
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            // var ps = new PizzaSuprema();
            // ps.PrepararPizzaTemplateMethod();
            var ph = new PizzaHawaiana();
            ph.PrepararPizzaTemplateMethod();
        }
    }
}
